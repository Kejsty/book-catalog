import dotenv from "dotenv";
import { App } from "./app";
import { SqliteRepository } from "./storage/sqlite/MasterStorage";

async function main() {
  let db = new SqliteRepository();
  await db.init("/tmp/database.db");
  // initialize server app
  const server = new App(db);
  server.init("/api");
  console.log("getting port");
  let port: number =
    parseInt(process.env.APP_PORT ? process.env.APP_PORT : "5000", 10) || 5000;
  await server.run(port);
}
// load the environment variables from the .env file
dotenv.config({
  path: ".env",
});

main();
