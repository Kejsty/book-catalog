import { BookRepository } from "../models/BookRepository";
import { Book } from "../models/Book";
import { Unstored } from "../models/DB";

export class BookController {
  private _storage: BookRepository;
  constructor(storage: BookRepository) {
    this._storage = storage;
  }

  async addNewBook(newBook: Unstored<Book>) {
    return await this._storage.insertBook(newBook);
  }

  async updateBook(bookID: number, uppdate: Unstored<Book>) {
    return await this._storage.updateBook(uppdate, bookID);
  }

  async findAll() {
    return await this._storage.getAllBooks();
  }

  async findOneById(bookID: number) {
    return await this._storage.findBookById(bookID);
  }

  async deleteBook(bookID: number) {
    return await this._storage.deleteBookById(bookID);
  }

  async search(substring: string) {
    return await this._storage.search(substring);
  }
}
