import { Unstored } from "../models/DB";
import { AuthorRepository } from "../models/AuthorRepository";
import { Author } from "../models/Author";

export class AuthorController {
  private _storage: AuthorRepository;
  constructor(storage: AuthorRepository) {
    this._storage = storage;
  }

  async addNewAuthor(newAuthor: Unstored<Author>) {
    return await this._storage.insertAuthor(newAuthor);
  }

  async updateAuthor(bookID: number, uppdate: Unstored<Author>) {
    return await this._storage.updateAuthor(uppdate, bookID);
  }

  async findAll() {
    return await this._storage.getAllAuthors();
  }

  async findOneById(bookID: number) {
    return await this._storage.findAuthorById(bookID);
  }

  async deleteAuthor(authorID: number) {
    return await this._storage.deleteAuthorById(authorID);
  }

  async search(substring: string) {
    return await this._storage.search(substring);
  }

  async books(author_id: number) {
    return await this._storage.findBooksByAuthor(author_id);
  }
}
