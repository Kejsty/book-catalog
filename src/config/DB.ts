export const BOOK_DB: string = "books";
export const AUTHOR_DB: string = "authors";
export const AUTHOR_BOOK_REL_DB: string = "book_author";

export const CREATE_BOOKS_DB: string = `
CREATE TABLE IF NOT EXISTS ${BOOK_DB} (
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    title text NOT NULL UNIQUE,
    description text
)`;

export const CREATE_AUTHORS_DB: string = `
CREATE TABLE  IF NOT EXISTS  ${AUTHOR_DB} (
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    name text NOT NULL UNIQUE
  )`;

export const CREATE_AUTHOR_BOOK_REL_DB: string = `
CREATE TABLE IF NOT EXISTS ${AUTHOR_BOOK_REL_DB} (
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    book_id INTEGER,
    author_id INTEGER,
    CONSTRAINT fk_book FOREIGN KEY (book_id) REFERENCES ${BOOK_DB} (id) ON DELETE CASCADE,
    CONSTRAINT fk_author FOREIGN KEY (author_id)  REFERENCES ${AUTHOR_DB} (id) ON DELETE CASCADE
)`;
