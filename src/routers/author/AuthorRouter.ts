import { NextFunction, Request, Response, Router } from "express";
import { Unstored } from "../../models/DB";
import { Author } from "../../models/Author";
import bodyParser from "body-parser";
import { AuthorController } from "../../controllers/AuthorController";
import { AuthorRepository } from "../../models/AuthorRepository";

export class AuthorRouter {
  private _router = Router();
  private _controller: AuthorController;

  private jsonParser = bodyParser.json();

  get router() {
    return this._router;
  }

  constructor(storage: AuthorRepository) {
    this._controller = new AuthorController(storage);
    this._configure();
  }

  private _configure() {
    this._router.get(
      "/all",
      async (req: Request, res: Response, next: NextFunction) => {
        try {
          const result = await this._controller.findAll();
          res.status(200).json(result);
        } catch (error) {
          next(error);
        }
      }
    );

    this._router.get(
      "/get/:id",
      async (req: Request, res: Response, next: NextFunction) => {
        const id: number = parseInt(req.params.id, 10);
        try {
          const result = await this._controller.findOneById(id);
          res.status(200).json(result);
        } catch (error) {
          next(error);
        }
      }
    );

    this._router.post(
      "/add",
      this.jsonParser,
      async (req: Request, res: Response, next: NextFunction) => {
        try {
          let toAdd: Unstored<Author> = req.body;
          const result = await this._controller.addNewAuthor(toAdd);
          res.status(201).json(result);
        } catch (error) {
          next(error);
        }
      }
    );

    this._router.post(
      "/edit/:id",
      this.jsonParser,
      async (req: Request, res: Response, next: NextFunction) => {
        try {
          let toEdit: Unstored<Author> = req.body;
          const id: number = parseInt(req.params.id, 10);
          await this._controller.updateAuthor(id, toEdit);
          const result = await this._controller.findOneById(id);
          res.status(200).json(result);
        } catch (error) {
          next(error);
        }
      }
    );

    this._router.delete(
      "/delete/:id",
      async (req: Request, res: Response, next: NextFunction) => {
        try {
          const id: number = parseInt(req.params.id, 10);
          const result = await this._controller.deleteAuthor(id);
          res.status(200).json(result);
        } catch (error) {
          next(error);
        }
      }
    );

    this._router.get(
      "/search/:pattern",
      async (req: Request, res: Response, next: NextFunction) => {
        try {
          const result = await this._controller.search(req.params.pattern);
          res.status(200).json(result);
        } catch (error) {
          next(error);
        }
      }
    );

    this._router.get(
      "/books/:id",
      async (req: Request, res: Response, next: NextFunction) => {
        try {
          const id: number = parseInt(req.params.id, 10);
          const result = await this._controller.books(id);
          res.status(200).json(result);
        } catch (error) {
          next(error);
        }
      }
    );
  }
}
