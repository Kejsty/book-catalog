import { Router } from "express";
import { BookRouter } from "./book/BookRouter";
import { AuthorRouter } from "./author/AuthorRouter";
import { Repository } from "../models/Repository";

export class MasterRouter {
  private _router = Router();
  private _bookRouter: BookRouter;
  private _autorRouter: AuthorRouter;

  get router() {
    return this._router;
  }

  constructor(repository: Repository) {
    this._bookRouter = new BookRouter(repository.Books());
    this._autorRouter = new AuthorRouter(repository.Authors());
    this._configure();
  }

  /**
   * Connect routes to their matching routers.
   */
  private _configure() {
    this._router.use("/books", this._bookRouter.router);
    this._router.use("/authors", this._autorRouter.router);
  }
}
