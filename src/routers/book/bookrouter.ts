import { NextFunction, Request, Response, Router } from "express";
import { BookController } from "../../controllers/BookConntroller";
import { BookRepository } from "../../models/BookRepository";
import { Book } from "../../models/Book";
import { Unstored } from "../../models/DB";
import bodyParser from "body-parser";

export class BookRouter {
  private _router = Router();
  private _controller: BookController;

  private jsonParser = bodyParser.json();

  get router() {
    return this._router;
  }

  constructor(storage: BookRepository) {
    this._controller = new BookController(storage);
    this._configure();
  }

  private _configure() {
    this._router.get(
      "/all",
      async (req: Request, res: Response, next: NextFunction) => {
        try {
          const result = await this._controller.findAll();
          res.status(200).json(result);
        } catch (error) {
          next(error);
        }
      }
    );

    this._router.get(
      "/get/:id",
      async (req: Request, res: Response, next: NextFunction) => {
        const id: number = parseInt(req.params.id, 10);
        try {
          const result = await this._controller.findOneById(id);
          res.status(200).json(result);
        } catch (error) {
          next(error);
        }
      }
    );

    this._router.post(
      "/add",
      this.jsonParser,
      async (req: Request, res: Response, next: NextFunction) => {
        try {
          let toAdd: Unstored<Book> = req.body;
          const result = await this._controller.addNewBook(toAdd);
          res.status(201).json(result);
        } catch (error) {
          next(error);
        }
      }
    );

    this._router.post(
      "/edit/:id",
      this.jsonParser,
      async (req: Request, res: Response, next: NextFunction) => {
        try {
          let toEdit: Unstored<Book> = req.body;
          const id: number = parseInt(req.params.id, 10);
          await this._controller.updateBook(id, toEdit); //edit
          const result = await this._controller.findOneById(id);
          res.status(200).json(result);
        } catch (error) {
          next(error);
        }
      }
    );

    this._router.delete(
      "/delete/:id",
      async (req: Request, res: Response, next: NextFunction) => {
        try {
          const id: number = parseInt(req.params.id, 10);
          await this._controller.deleteBook(id);
          res.status(200).json({ message: "ok" });
        } catch (error) {
          next(error);
        }
      }
    );

    this._router.get(
      "/search/:pattern",
      async (req: Request, res: Response, next: NextFunction) => {
        try {
          const result = await this._controller.search(req.params.pattern);
          res.status(200).json(result);
        } catch (error) {
          next(error);
        }
      }
    );
  }
}
