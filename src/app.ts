import { MasterRouter } from "./routers/MasterRouter";
import ErrorHandler from "./models/ErrorHandler";
import express, { Request, Response, NextFunction, Express } from "express";
import bodyParser from "body-parser";
import { Server } from "http";
import { Repository } from "./models/Repository";

export class App {
  public server: Express;
  router: MasterRouter;

  constructor(repository: Repository) {
    this.server = express();
    this.router = new MasterRouter(repository);
  }

  init(path: string) {
    this.server
      .use(path, this.router.router)
      .use(express.json())
      .use(bodyParser.json());
    // make server app handle any error
    this.server.use(
      (err: ErrorHandler, req: Request, res: Response, next: NextFunction) => {
        res.status(err.statusCode || 500).json({
          status: "error",
          statusCode: err.statusCode,
          message: err.message,
        });
      }
    );
  }

  async run(port: number): Promise<Server> {
    return this.server.listen(port, () =>
      console.log(`> Listening on port ${port}`)
    );
  }
}
