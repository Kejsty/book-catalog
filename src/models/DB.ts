import { Author } from "./Author";
import { Book } from "./Book";

export type BookDB = Omit<Book, "authors">;

export type AuthorDB = Author;

export interface AuthorBookDB {
  id: number;
  author_id: number;
  book_id: number;
}

//Book should extend
export interface StoredEntity {
  id: number;
}

//Unstored represents item that have not been stored yet - no id
export type Unstored<T extends StoredEntity> = Omit<T, "id">;
