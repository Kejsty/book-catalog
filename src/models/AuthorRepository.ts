import { Author } from "./Author";
import { Book } from "./Book";
import { Unstored, AuthorDB } from "./DB";

export interface AuthorRepository {
  insertAuthor(author: Unstored<Author>): Promise<Author>;

  updateAuthor(update: Unstored<Author>, id: number): Promise<void>;

  findAuthorById(id: number): Promise<Author | null>;

  getAllAuthors(): Promise<Author[]>;

  findBooksByAuthor(id: number): Promise<Book[]>;

  deleteAuthorById(id: number): Promise<void>;

  search(substring: string): Promise<Author[]>;
}
