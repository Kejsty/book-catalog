import { Author } from "./Author";
import { Book } from "./Book";
import { Unstored, BookDB } from "./DB";

export interface BookRepository {
  insertBook(book: Unstored<Book>): Promise<Book>;

  updateBook(update: Unstored<Book>, id: number): Promise<void>;

  findBookById(id: number): Promise<Book | null>;

  findBookByTitle(id: string): Promise<Book | null>;

  getAllBooks(): Promise<Book[]>;

  deleteBookById(id: number): Promise<void>;

  search(substring: string): Promise<Book[]>;
}
