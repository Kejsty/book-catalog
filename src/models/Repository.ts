import { Author } from "./Author";
import { Book } from "./Book";
import { Unstored, AuthorDB } from "./DB";
import { BookRepository } from "./BookRepository";
import { AuthorRepository } from "./AuthorRepository";

export interface Repository {
  Books(): BookRepository;
  Authors(): AuthorRepository;
}
