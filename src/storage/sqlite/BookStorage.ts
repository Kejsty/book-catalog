import { BookDB, Unstored, AuthorDB } from "../../models/DB";

import { Database } from "sqlite";
import { BookRepository } from "../../models/BookRepository";
import { Book } from "../../models/Book";
import { Author } from "../../models/Author";
import * as DB from "../../config/DB";

export class SqliteBookRepository implements BookRepository {
  private sqlite: Database;

  constructor(sqlite: Database) {
    this.sqlite = sqlite;
  }

  async create(): Promise<null> {
    try {
      await this.sqlite.exec(DB.CREATE_BOOKS_DB);
      await this.sqlite.exec(DB.CREATE_AUTHORS_DB);
      await this.sqlite.exec(DB.CREATE_AUTHOR_BOOK_REL_DB);
    } catch (error) {
      console.log("Failed to create DBs: " + error);
    }
    return null;
  }

  async relate(book_id: number, authors: Unstored<Author>[]): Promise<void> {
    try {
      authors.forEach(async (author: Unstored<Author>) => {
        await this.sqlite.run(
          `INSERT into ${DB.AUTHOR_BOOK_REL_DB}(book_id, author_id) values(${book_id}, (select id from authors where name='${author.name}'))`
        );
      });
    } catch (error) {
      throw new Error("Could not add book-author relations: " + error);
    }
  }

  async insertBook(book: Unstored<Book>): Promise<Book> {
    try {
      let resp = await this.sqlite.run(
        `INSERT into ${DB.BOOK_DB}(title, 'description') values('${book.title}', '${book.description}')`
      );

      if (!resp.lastID) {
        throw new Error("Adding author haven't generated id");
      }

      await this.insertNonexistentAuthors(book.authors);
      await this.relate(resp.lastID.valueOf(), book.authors);

      let newbook: Book = {
        ...book,
        ...{ id: resp.lastID.valueOf() },
        ...{ authors: await this.getAuthors(resp.lastID.valueOf()) },
      };
      return newbook;
    } catch (error) {
      throw new Error("Could not add new book: " + error);
    }
  }

  private async insertNonexistentAuthors(
    authors: Unstored<Author>[]
  ): Promise<void> {
    try {
      authors.forEach(async (author) => {
        await this.sqlite.run(
          `INSERT OR IGNORE INTO ${DB.AUTHOR_DB}(name) values('${author.name}')`
        );
      });
    } catch (error) {
      throw new Error("Could not add new author: " + error);
    }
  }

  async updateBook(update: Unstored<Book>, id: number): Promise<void> {
    try {
      if (update.title && update.title.length > 0) {
        await this.sqlite.run(
          `update ${DB.BOOK_DB} set title='${update.title}' where id = ${id}`
        );
      }

      if (update.description && update.description.length > 0) {
        await this.sqlite.run(
          `update ${DB.BOOK_DB} set description='${update.description}' where id = ${id}`
        );
      }
    } catch (error) {
      throw new Error("Couldn't load data from database : " + error);
    }
  }

  private async getAuthors(user_id: number): Promise<AuthorDB[]> {
    try {
      let authorsDB: AuthorDB[] = await this.sqlite.all(
        `SELECT a.id, a.name from ${DB.AUTHOR_BOOK_REL_DB} ba inner join ${DB.AUTHOR_DB} a on ba.author_id = a.id where ba.book_id=?`,
        user_id
      );
      return authorsDB;
    } catch (error) {
      throw new Error("Couldn't load data from database " + error);
    }
  }

  async getAllAuthors(): Promise<Author[]> {
    try {
      let authorsDB: AuthorDB[] = await this.sqlite.all(
        `SELECT * from ${DB.AUTHOR_DB}`
      );
      return authorsDB;
    } catch (error) {
      throw new Error("Couldn't load data from database.");
    }
  }

  async findBookById(id: number): Promise<Book> {
    try {
      let bookDB: BookDB | undefined = await this.sqlite.get(
        `SELECT * from ${DB.BOOK_DB} where id = ?`,
        id
      );
      if (bookDB) {
        let book: Book = {
          ...bookDB,
          ...{ authors: await this.getAuthors(bookDB.id) },
        };
        return book;
      }
    } catch (error) {
      throw new Error("Couldn't load data from database: " + error);
    }

    throw new Error(`Book ${id} not present`);
  }

  async findBookByTitle(title: string): Promise<Book | null> {
    try {
      let bookDB: BookDB | undefined = await this.sqlite.get(
        `SELECT * from ${DB.BOOK_DB} where title = ?`,
        title
      );

      if (bookDB) {
        let book: Book = {
          ...bookDB,
          ...{ authors: await this.getAuthors(bookDB.id) },
        };

        return book;
      }
    } catch (error) {
      throw new Error("Couldn't load data from database.");
    }
    throw new Error(`Book ${title} not present`);
  }

  async getAllBooks(): Promise<Book[]> {
    try {
      let booksDB: BookDB[] = await this.sqlite.all(
        `Select * from ${DB.BOOK_DB}`
      );
      let books = new Array<Book>();
      await Promise.all(
        booksDB.map(async (book: BookDB) => {
          books.push({
            ...book,
            ...{ authors: await this.getAuthors(book.id) },
          });
        })
      );
      return books;
    } catch (error) {
      throw new Error(`Could not parse DB data: ${error}`);
    }
  }

  async deleteBookById(id: number): Promise<void> {
    try {
      const result = await this.sqlite.run(
        `DELETE from ${DB.BOOK_DB} where id = ${id}`
      );
    } catch (error) {
      throw new Error("Could not delete book: " + error);
    }
  }

  async search(substring: string): Promise<Book[]> {
    try {
      let booksDB: BookDB[] | undefined = await this.sqlite.all(
        `SELECT * from ${DB.BOOK_DB} where LOWER(title) LIKE LOWER('%${substring}%') OR LOWER(description) LIKE LOWER('%${substring}%') LIMIT 10`
      );

      if (!booksDB) {
        return new Array<Book>();
      }

      let books = new Array<Book>();
      await Promise.all(
        booksDB.map(async (book: BookDB) => {
          books.push({
            ...book,
            ...{ authors: await this.getAuthors(book.id) },
          });
        })
      );

      return books;
    } catch (error) {
      throw new Error("Couldn't load data from database: " + error);
    }
  }
}
