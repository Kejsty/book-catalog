import { SqliteBookRepository } from "./BookStorage";
import { SqliteAuthorRepository } from "./AuthorStorage";
import sqlite3 from "sqlite3";
import { open, Database } from "sqlite";
import { Repository } from "../../models/Repository";
import { BookRepository } from "../../models/BookRepository";
import { AuthorRepository } from "../../models/AuthorRepository";

export class SqliteRepository implements Repository {
  private db: Database | undefined;
  private _books: SqliteBookRepository | undefined;
  private _authors: SqliteAuthorRepository | undefined;

  constructor() {}

  public async init(path: string): Promise<void> {
    this.db = await open({
      filename: path,
      driver: sqlite3.Database,
    });

    this._books = new SqliteBookRepository(this.db);
    await this._books.create();

    this._authors = new SqliteAuthorRepository(this.db);
    await this._authors.create();
  }

  public Books(): BookRepository {
    if (!this._books) {
      throw Error("SQLITE repository not initialized");
    }
    return this._books;
  }

  public Authors(): AuthorRepository {
    if (!this._authors) {
      throw Error("SQLITE repository not initialized");
    }
    return this._authors;
  }

  close() {
    if (this.db) {
      this.db.close();
    }
  }
}
