import { BookDB, Unstored, AuthorDB, StoredEntity } from "../../models/DB";
import { Database } from "sqlite";
import { AuthorRepository } from "../../models/AuthorRepository";
import { Book } from "../../models/Book";
import { Author } from "../../models/Author";
import * as DB from "../../config/DB";

export class SqliteAuthorRepository implements AuthorRepository {
  private sqlite: Database;

  constructor(sqlite: Database) {
    this.sqlite = sqlite;
  }

  async create(): Promise<null> {
    try {
      await this.sqlite.exec(DB.CREATE_BOOKS_DB);
      await this.sqlite.exec(DB.CREATE_AUTHORS_DB);
      await this.sqlite.exec(DB.CREATE_AUTHOR_BOOK_REL_DB);
    } catch (error) {
      console.log("Failed to create DBs: " + error);
    }
    return null;
  }

  async insertAuthor(author: Unstored<Author>): Promise<Author> {
    try {
      let resp = await this.sqlite.run(
        `INSERT into ${DB.AUTHOR_DB}(name) values('${author.name}')`
      );

      let newauthor: Author = {
        ...author,
        ...{ id: resp.lastID ? resp.lastID.valueOf() : 0 },
      };
      return newauthor;
    } catch (error) {
      throw new Error("Could add new author: " + error);
    }
  }

  async updateAuthor(update: Unstored<Author>, id: number): Promise<void> {
    try {
      if (update.name && update.name.length > 0) {
        await this.sqlite.run(
          `update ${DB.AUTHOR_DB} set name='${update.name}' where id=${id}`
        );
      }
    } catch (error) {
      throw new Error("Couldn't load data from database " + error);
    }
  }

  private async getAuthors(user_id: number): Promise<AuthorDB[]> {
    try {
      let authorsDB: AuthorDB[] = await this.sqlite.all(
        `SELECT a.id, a.name from ${DB.AUTHOR_BOOK_REL_DB} ba inner join ${DB.AUTHOR_DB} a on ba.author_id = a.id where ba.book_id=?`,
        user_id
      );
      return authorsDB;
    } catch (error) {
      throw new Error("Couldn't load data from database " + error);
    }
  }

  async getAllAuthors(): Promise<Author[]> {
    try {
      let authorsDB: AuthorDB[] = await this.sqlite.all(
        `SELECT * from ${DB.AUTHOR_DB}`
      );
      return authorsDB;
    } catch (error) {
      throw new Error("Couldn't load data from database.");
    }
  }

  async findBooksByAuthor(author_id: number): Promise<Book[]> {
    try {
      let bookIDs: StoredEntity[] = await this.sqlite.all(
        `SELECT b.id from ${DB.AUTHOR_BOOK_REL_DB} ba inner join ${DB.BOOK_DB} b on ba.book_id = b.id where ba.author_id=?`,
        author_id
      );

      let books = new Array<Book>();
      bookIDs.forEach(async (e: StoredEntity) => {
        books.push(await this.findBookById(e.id));
      });

      return books;
    } catch (error) {
      throw new Error("Couldn't load data from database.");
    }
  }

  async findBookById(id: number): Promise<Book> {
    try {
      let bookDB: BookDB | undefined = await this.sqlite.get(
        `SELECT * from ${DB.BOOK_DB} where id = ?`,
        id
      );
      if (!bookDB) {
        throw new Error(`Book ${id} not present`);
      }

      let book: Book = {
        ...bookDB,
        ...{ authors: await this.getAuthors(bookDB.id) },
      };
      return book;
    } catch (error) {
      throw new Error("Couldn't load data from database.");
    }
  }

  async getAllBooks(): Promise<Book[]> {
    try {
      let booksDB: BookDB[] = await this.sqlite.all(
        `Select * from ${DB.BOOK_DB}`
      );
      let books = new Array<Book>();
      booksDB.forEach(async (book: BookDB) => {
        books.push({ ...book, ...{ authors: await this.getAuthors(book.id) } });
      });
      return books;
    } catch (error) {
      throw new Error(`Could not parse DB data: ${error}`);
    }
  }

  async findAuthorById(id: number): Promise<Author> {
    try {
      let author: AuthorDB | undefined = await this.sqlite.get(
        `SELECT * from ${DB.AUTHOR_DB} where id=?`,
        id
      );
      if (author) {
        return author;
      }
    } catch (error) {
      throw new Error("Couldn't load data from database.");
    }

    throw new Error(`Author ${id} not present`);
  }

  async deleteAuthorById(id: number): Promise<void> {
    try {
      const result = await this.sqlite.run(
        `DELETE from ${DB.AUTHOR_DB} where id = ${id}`
      );
    } catch (error) {
      throw new Error("Could not delete book: " + error);
    }
  }

  async search(substring: string): Promise<Author[]> {
    try {
      let authors: AuthorDB[] | undefined = await this.sqlite.all(
        `SELECT * from ${DB.AUTHOR_DB} where LOWER(name) LIKE LOWER('%${substring}%') LIMIT 10`
      );

      if (!authors) {
        return new Array<Author>();
      }

      return authors;
    } catch (error) {
      throw new Error("Couldn't load data from database.");
    }
  }
}
