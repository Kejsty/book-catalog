import chai from "chai";
import chaiHttp from "chai-http";
import { Server } from "http";
import { App } from "../src/app";
import { open, Database } from "sqlite";
import sqlite3 from "sqlite3";
import { SqliteBookRepository } from "../src/storage/sqlite/BookStorage";
import { SqliteAuthorRepository } from "../src/storage/sqlite/AuthorStorage";
import axios from "axios";
import dotenv from "dotenv";
import fs from "fs";
import { fstat } from "fs";
import { Book } from "../src/models/Book";
import { Unstored, BookDB } from "../src/models/DB";
import { Author } from "../src/models/Author";
import { SqliteRepository } from "../src/storage/sqlite/MasterStorage";
import { Repository } from "../src/models/Repository";

const book_test_port = 5000;
const bookA = {
  title: "Anna Karenina",
  description:
    "A complex novel in eight parts, with more than a dozen major characters, it is spread over more than 800 pages (depending on the translation and publisher), typically contained in two volumes. It deals with themes of betrayal, faith, family, marriage, Imperial Russian society, desire, and rural vs. city life",
  authors: [{ name: "Lev Nikolayevich Tolstoy" }],
};
const bookB = {
  title: "Tiny Pretty Things",
  description:
    "Gigi, Bette, and June, three top students at an exclusive Manhattan ballet school, have seen their fair share of drama",
  authors: [{ name: "Sona Charaipotra" }, { name: "Dhonielle Clayton" }],
};

let b1Stored: Book;
let b2Stored: Book;
chai.use(chaiHttp);

describe("books tests", async () => {
  let app: App;
  let db: SqliteRepository;

  before(async () => {
    db = new SqliteRepository();
    await db.init("/tmp/book_test_database.db");
    // initialize server app
    app = new App(db);
    app.init("/api");

    return app.run(book_test_port);
  });

  it("add book, 1 author", (done) => {
    chai
      .request(app.server)
      .post("/api/books/add")
      .send(bookA)
      .end((err, res) => {
        chai.expect(res.status).to.eql(201);
        chai.expect(res.body).to.not.eql(undefined);
        chai.expect(res.body.id).to.be.a("number");
        chai.expect(res.body.id).to.eql(1);
        b1Stored = res.body;
        compare(bookA, b1Stored);
        compareAuthors(bookA.authors, b1Stored.authors);
        done();
      });
  });

  it("add book, 2 authors", (done) => {
    chai
      .request(app.server)
      .post("/api/books/add")
      .send(bookB)
      .end((err, res) => {
        chai.expect(res.status).to.eql(201);
        chai.expect(res.body).to.not.eql(undefined);
        chai.expect(res.body.id).to.be.a("number");
        chai.expect(res.body.id).to.eql(2);
        b2Stored = res.body;
        compare(bookA, b1Stored);
        compareAuthors(bookA.authors, b1Stored.authors);
        done();
      });
  });

  it("get all", (done) => {
    chai
      .request(app.server)
      .get("/api/books/all")
      .end((err, res) => {
        chai.expect(res.status).to.eql(200);
        chai.expect(res.body).to.not.eql(undefined);
        chai.expect(res.body).to.be.a("array");
        chai.expect(res.body.length).to.eql(2);
        chai.expect(res.body).to.have.deep.members([b1Stored, b2Stored]);
        done();
      });
  });

  it("get first", (done) => {
    chai
      .request(app.server)
      .get("/api/books/get/1")
      .end((err, res) => {
        chai.expect(res.status).to.eql(200);
        chai.expect(res.body).to.not.eql(undefined);
        chai.expect(res.body).to.be.a("object");
        chai.expect(res.body.id).to.be.a("number");
        chai.expect(res.body.id).to.eql(1);
        let b: Book = res.body;
        compare(bookA, b);
        compareAuthors(bookA.authors, b.authors);
        done();
      });
  });

  it("search", (done) => {
    chai
      .request(app.server)
      .get("/api/books/search/anna")
      .end((err, res) => {
        chai.expect(res.status).to.eql(200);
        chai.expect(res.body).to.not.eql(undefined);
        chai.expect(res.body).to.be.a("array");
        chai.expect(res.body.length).to.eql(1);
        chai.expect(res.body).to.have.deep.members([b1Stored]);
        done();
      });
  });

  it("edit book, title only", (done) => {
    let editedA = { ...bookA, ...{ title: "Anna Kareninova" } };
    chai
      .request(app.server)
      .post(`/api/books/edit/${b1Stored.id}`)
      .send({ title: "Anna Kareninova" })
      .end((err, res) => {
        chai.expect(res.status).to.eql(200);
        chai.expect(res.body).to.not.eql(undefined);
        chai.expect(res.body.id).to.be.a("number");
        chai.expect(res.body.id).to.eql(1);
        b1Stored = res.body;
        compare(editedA, b1Stored);
        compareAuthors(bookA.authors, b1Stored.authors);
        done();
      });
  });

  it("edit book, title and description only", (done) => {
    let update = { title: "Tiny Pretty Things", description: "new desc" };
    let editedB = {
      ...bookB,
      ...update,
    };
    chai
      .request(app.server)
      .post(`/api/books/edit/${b2Stored.id}`)
      .send(update)
      .end((err, res) => {
        chai.expect(res.status).to.eql(200);
        chai.expect(res.body).to.not.eql(undefined);
        chai.expect(res.body.id).to.be.a("number");
        chai.expect(res.body.id).to.eql(b2Stored.id);
        b2Stored = res.body;
        compare(editedB, b2Stored);
        compareAuthors(editedB.authors, b2Stored.authors);
        done();
      });
  });

  it("delete book", (done) => {
    chai
      .request(app.server)
      .delete(`/api/books/delete/${b1Stored.id}`)
      .end((err, res) => {
        chai.expect(res.status).to.eql(200);
        chai.expect(res.body).to.not.eql(undefined);
        done();
      });
  });

  it("get all after delete", (done) => {
    chai
      .request(app.server)
      .get("/api/books/all")
      .end((err, res) => {
        chai.expect(res.status).to.eql(200);
        chai.expect(res.body).to.not.eql(undefined);
        chai.expect(res.body).to.be.a("array");
        chai.expect(res.body.length).to.eql(1);
        chai.expect(res.body).to.have.deep.members([b2Stored]);
        done();
      });
  });

  after(() => {
    db.close();
    fs.unlinkSync("/tmp/book_test_database.db");
  });
});

function compare(b1: Unstored<BookDB>, b2: Book) {
  chai.expect(b1.title).to.eql(b2.title);
  chai.expect(b1.description).to.eql(b2.description);
}

function compareAuthors(a1: Unstored<Author>[], a2: Unstored<Author>[]) {
  chai.expect(a1.length).to.eql(a2.length);
  chai
    .expect(
      a1.map((author: Unstored<Author>) => {
        return author.name;
      })
    )
    .to.include.all.members(
      a2.map((author: Unstored<Author>) => {
        return author.name;
      })
    );
}
