import chai from "chai";
import chaiHttp from "chai-http";
import { Server } from "http";
import { App } from "../src/app";
import { open, Database } from "sqlite";
import sqlite3 from "sqlite3";
import { SqliteBookRepository } from "../src/storage/sqlite/BookStorage";
import { SqliteAuthorRepository } from "../src/storage/sqlite/AuthorStorage";
import axios from "axios";
import dotenv from "dotenv";
import fs from "fs";
import { fstat } from "fs";
import { Book } from "../src/models/Book";
import { Unstored, BookDB } from "../src/models/DB";
import { Author } from "../src/models/Author";
import { SqliteRepository } from "../src/storage/sqlite/MasterStorage";

const autohor_test_port = 5001;
const bookB = {
  title: "Tiny Pretty Things",
  description:
    "Gigi, Bette, and June, three top students at an exclusive Manhattan ballet school, have seen their fair share of drama",
  authors: [{ name: "Sona Charaipotra" }, { name: "Dhonielle Clayton" }],
};
const authorA = { name: "Lev Nikolayevich Tolstoy" };

let b2Stored: Book;
let a1Stored: Author;

chai.use(chaiHttp);

describe("author tests", async () => {
  let app: App;
  let db: SqliteRepository;

  before(async () => {
    db = new SqliteRepository();
    await db.init("/tmp/author_test_database.db");
    // initialize server app
    app = new App(db);
    app.init("/api");

    return app.run(autohor_test_port);
  });

  it("add book, 2 authors, check", (done) => {
    chai
      .request(app.server)
      .post("/api/books/add")
      .send(bookB)
      .end((err, res) => {
        chai.expect(res.status).to.eql(201);
        chai.expect(res.body).to.not.eql(undefined);
        chai.expect(res.body.id).to.be.a("number");
        chai.expect(res.body.id).to.eql(1);
        b2Stored = res.body;
        compare(bookB, b2Stored);
        compareAuthors(bookB.authors, b2Stored.authors);
        done();
      });
  });

  it("add author", (done) => {
    chai
      .request(app.server)
      .post("/api/authors/add")
      .send(authorA)
      .end((err, res) => {
        chai.expect(res.status).to.eql(201);
        chai.expect(res.body).to.not.eql(undefined);
        chai.expect(res.body.id).to.be.a("number");
        chai.expect(res.body.id).to.eql(3);
        a1Stored = res.body;
        chai.expect(authorA.name).to.eql(a1Stored.name);
        done();
      });
  });

  it("get all", (done) => {
    chai
      .request(app.server)
      .get("/api/authors/all")
      .end((err, res) => {
        chai.expect(res.status).to.eql(200);
        chai.expect(res.body).to.not.eql(undefined);
        chai.expect(res.body).to.be.a("array");
        chai.expect(res.body.length).to.eql(3);
        chai
          .expect(res.body)
          .to.have.deep.members(
            new Array<Author>(...b2Stored.authors, a1Stored)
          );
        done();
      });
  });

  it("get one", (done) => {
    chai
      .request(app.server)
      .get(`/api/authors/get/${a1Stored.id}`)
      .end((err, res) => {
        chai.expect(res.status).to.eql(200);
        chai.expect(res.body).to.not.eql(undefined);
        chai.expect(res.body).to.be.a("object");
        chai.expect(res.body.id).to.be.a("number");
        chai.expect(res.body.id).to.eql(a1Stored.id);
        let b: Author = res.body;
        chai.expect(b).to.deep.equal(a1Stored);
        done();
      });
  });

  it("search", (done) => {
    chai
      .request(app.server)
      .get("/api/authors/search/Tol")
      .end((err, res) => {
        chai.expect(res.status).to.eql(200);
        chai.expect(res.body).to.not.eql(undefined);
        chai.expect(res.body).to.be.a("array");
        chai.expect(res.body.length).to.eql(1);
        chai.expect(res.body).to.have.deep.members([a1Stored]);
        done();
      });
  });

  it("edit author", (done) => {
    let editedA = { name: "Anna Kareninova" };
    chai
      .request(app.server)
      .post(`/api/authors/edit/${a1Stored.id}`)
      .send(editedA)
      .end((err, res) => {
        chai.expect(res.status).to.eql(200);
        chai.expect(res.body).to.not.eql(undefined);
        chai.expect(res.body.id).to.be.a("number");
        chai.expect(res.body.id).to.eql(a1Stored.id);
        a1Stored = res.body;
        chai.expect(editedA.name).to.eql(a1Stored.name);
        done();
      });
  });

  it("delete author", (done) => {
    chai
      .request(app.server)
      .delete(`/api/authors/delete/${a1Stored.id}`)
      .end((err, res) => {
        chai.expect(res.status).to.eql(200);
        chai.expect(res.body).to.not.eql(undefined);
        done();
      });
  });

  it("get all after delete", (done) => {
    chai
      .request(app.server)
      .get("/api/authors/all")
      .end((err, res) => {
        chai.expect(res.status).to.eql(200);
        chai.expect(res.body).to.not.eql(undefined);
        chai.expect(res.body).to.be.a("array");
        chai.expect(res.body.length).to.eql(2);
        chai.expect(res.body).to.have.deep.members(b2Stored.authors);
        done();
      });
  });

  after(() => {
    db.close();
    fs.unlinkSync("/tmp/author_test_database.db");
  });
});

function compare(b1: Unstored<BookDB>, b2: Book) {
  chai.expect(b1.title).to.eql(b2.title);
  chai.expect(b1.description).to.eql(b2.description);
}

function compareAuthors(a1: Unstored<Author>[], a2: Unstored<Author>[]) {
  chai.expect(a1.length).to.eql(a2.length);
  chai
    .expect(
      a1.map((author: Unstored<Author>) => {
        return author.name;
      })
    )
    .to.include.all.members(
      a2.map((author: Unstored<Author>) => {
        return author.name;
      })
    );
}
