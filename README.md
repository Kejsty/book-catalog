# Book Catalog

## DB
I have selected SQL as storage for users.  It allows to model the n..m books-authors relationship and also allows searching. Note that I implemented SQLlite storage, only for 3 reasons:
- Dependency convenience
- I currently only have OSX, which failed to run docker, therefore I couldn't run PostgreSQL in docker
- I would write SQLite adapter anyway for test

Ideally, I would use PostgreSQL. However, the adapter for PostgreSQL should look more-less the same.

## Endpoints: 

/api/books is api endpoint for books. it allows:
- create : `POST` /api/books/add
- edit : `POST` /api/books/edit/$id
- delete : `DELETE` /api/books/delete/$id
- get : `GET` /api/books/get/$id
- get all : `GET` /api/books/all
- search : `GET` /api/books/search/$pattern

If `POST`, there is expected body in form of an object: 

``` .json
{
  title: "Anna Karenina",
  description:
    "A complex novel in eight parts, with more than a dozen major characters, it is spread over more than 800 pages (depending on the translation and publisher), typically contained in two volumes. It deals with themes of betrayal, faith, family, marriage, Imperial Russian society, desire, and rural vs. city life",
  authors: [{ name: "Lev Nikolayevich Tolstoy" }],
}
``` 

/api/authors is api endpoint for authors. it allows: 
- create : `POST` /api/authors/add
- edit : `POST` /api/authors/edit/$id
- delete : `DELETE` /api/authors/delete/$id
- get : `GET` /api/authors/get/$id
- get all : `GET` /api/authors/all
- search : `GET` /api/authors/search/$pattern


If `POST`, there is expected body in form of an object: 


``` .json
{ name: "Lev Nikolayevich Tolstoy" }
``` 

## Tests
Tests included in this package test only "sunshine scenarios", there are no tests for invalid inputs and the resulting error messages

## Other problems: 
- no possibility to edit authors of a book (e.g. their number etc.). There is the only possibility to change each author through `api/authors/edit` API endpoint.
- additional functionalities (get books of authors..) not tested

# Notes
Note that this is my first not-home-only project in Typescript. I can feel, that I'm not 100% about the right patters and standards used with his language. Moreover, I'm not sure how to
- correctly use constructors: since there are no awaits in construction as well as no destructor, I'm not sure how to handle RAII: for example DB creation. Contructor+init seem weird, but I assume that's the correct way?
- where to define interfaces: I'm sure I overused models. And overall, what is the expected structure. I jumped into the domain-boundary-infrastructure design but I'm not sure I liked it. 